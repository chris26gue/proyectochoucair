package com.udemy.automation.application.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Christian D Moreno G.
 */
@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfig {

    @Getter
    @Value("${app.url}")
    private String url;

    @Getter
    @Value("${app.username.choucair}")
    private String userName;

    @Getter
    @Value("${app.password.choucair}")
    private String password;

    @Getter
    @Value("${app.name}")
    private String name;

    @Getter
    @Value("${app.parent.unit}")
    private String parentUnit;
    
}
