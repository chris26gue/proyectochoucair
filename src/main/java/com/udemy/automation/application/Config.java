package com.udemy.automation.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Christian D Moreno G.
 */
@Configuration
@ComponentScan("com.udemy.automation.application")
public class Config {
}
