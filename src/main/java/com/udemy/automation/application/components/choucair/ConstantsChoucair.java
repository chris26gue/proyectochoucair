package com.udemy.automation.application.components.choucair;

public class ConstantsChoucair {
    public static final String LOGO_CHOUCAIR="//div[@id='LoginPanel']//img[@src='/demo/Content/site/images/serenity-logo-w-128.png']";
    public static final String LOGIN_TO_YOUR_ACCOUNT="//div[@id='LoginPanel']//h5[.='Login to your account']";
    public static final String FORGOT_PASSWORD="//form[@id='StartSharp_Membership_LoginPanel0_Form']//a[@href='/demo/Account/ForgotPassword']";
    public static final String INICIAR_SESION="/html//button[@id='StartSharp_Membership_LoginPanel0_LoginButton']";
    public static final String INPUT_USERNAME="/html//input[@id='StartSharp_Membership_LoginPanel0_Username']";
    public static final String INPUT_PASSWORD="/html//input[@id='StartSharp_Membership_LoginPanel0_Password']";
    public static final String BUTTON_TOGGLER="//button[@id='s-sidebar-toggler']/i[@class='fa fa-bars']";
    public static final String BUTTON_ORGANIZATION="//aside[@id='s-sidebar']//div[@class='s-sidebar-groups']/div[1]/ul[@class='s-sidebar-menu']/li[3]/ul[@class='s-sidebar-section-menu']//a[@href='#nav_menu1_3_1']/span[@class='s-sidebar-link-text']";
    public static final String BUTTON_BUSINESS_UNITS="//aside[@id='s-sidebar']//div[@class='s-sidebar-groups']/div[1]/ul[@class='s-sidebar-menu']/li[3]/ul[@class='s-sidebar-section-menu']/li[1]/ul//a[@href='/demo/Organization/BusinessUnit']/span[@class='s-sidebar-link-text']";
    public static final String BUTTON_NEW_BUSINESS_UNKT="/html//div[@id='GridDiv']/div[2]/div[@class='tool-buttons']//div[@class='buttons-inner']/div[1]/div[@class='button-outer']/span[@class='button-inner']";
    public static final String INPUT_NAME="/html//input[@id='Serenity_Pro_Organization_BusinessUnitDialog3_Name']";
    public static final String SELECT_PARENT_UNIT="//div[@id='Serenity_Pro_Organization_BusinessUnitDialog3_PropertyGrid']//div[@class='category']/div[2]/div[1]/a[@href='javascript:void(0)']/span[@class='select2-chosen']";
    public static final String BUTTON_SAVE="/html//div[@id='Serenity_Pro_Organization_BusinessUnitDialog3_Toolbar']/div[@class='tool-buttons']//div[@class='buttons-inner']/div[1]/div[@class='button-outer']/span[@class='button-inner']";
    public static final String INPUT_SEARCH_PARENT_UNIT="//div[@id='select2-drop']//input[@role='combobox']";
    public static final String BUTTON_MEETING="//aside[@id='s-sidebar']//div[@class='s-sidebar-groups']/div[1]/ul[@class='s-sidebar-menu']/li[3]/ul[@class='s-sidebar-section-menu']//a[@href='#nav_menu1_3_2']/span[@class='s-sidebar-link-text']";
    public static final String BUTTON_MEETINGS="//aside[@id='s-sidebar']//div[@class='s-sidebar-groups']/div[1]/ul[@class='s-sidebar-menu']/li[3]/ul[@class='s-sidebar-section-menu']/li[2]/ul//a[@href='/demo/Meeting/Meeting']/span[@class='s-sidebar-link-text']";
}
