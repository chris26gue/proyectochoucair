package com.udemy.automation.application.components.choucair.tasks;

import com.udemy.automation.application.components.choucair.ChoucairComponent;
import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

public class InsertCredentialsChoucair implements Task {

	// @Setter
	private String userName;

	// @Setter
	private String password;

	@Autowired
	private ChoucairComponent component;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(component.enterUserNameValue(userName), component.enterPasswordValue(password),
				component.clickButtonSingIn());
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
