package com.udemy.automation.application.components.choucair.tasks;

import com.udemy.automation.application.components.choucair.ChoucairComponent;
import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

public class InsertValuesNewBusinessUnitaChoucair implements Task {

    @Setter
    private String name;

    @Setter
    private String parentUnit;

    @Autowired
    private ChoucairComponent component;

    @Override
    public <T extends Actor> void performAs(T actor){
        actor.attemptsTo(
                component.enterName(name),
                component.selectOptionParentUnit(parentUnit),
                component.clickButtonSave()
        );
    }
    

}
