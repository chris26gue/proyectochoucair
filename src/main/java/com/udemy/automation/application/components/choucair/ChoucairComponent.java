package com.udemy.automation.application.components.choucair;

import com.udemy.automation.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.annotation.PostConstruct;

@PrototypeScope
public class ChoucairComponent {

    @Getter
    private Target iconChoucair;

    @Getter
    private Target LabelLoginToYourAccount;

    @Getter
    private Target buttonForgotPassword;

    @Getter
    private Target singIn;

    @Getter
    private Target inputUserName;

    @Getter
    private Target inputPassword;

    @Getter
    private Target buttonToggler;

    @Getter
    private Target buttonOrganization;

    @Getter
    private Target buttonBusinessUnits;

    @Getter
    private Target buttonNewBusinessUnits;

    @Getter
    private Target inputName;

    @Getter
    private Target parentUnit;

    @Getter
    private Target buttonSave;

    @Getter
    private Target inputSearchParentUnit;

    @Getter
    private Target buttonMeeting;

    public Performable enterUserNameValue(String username){
        Clear.field(inputUserName);
        return Enter.theValue(username).into(inputUserName).thenHit(Keys.TAB);
    }

    public Performable enterPasswordValue(String password){
        Clear.field(inputPassword);
        return Enter.theValue(password).into(inputPassword).thenHit(Keys.TAB);
    }

    public Performable enterName(String name){
       return Enter.theValue(name).into(inputName).thenHit(Keys.TAB).thenHit(Keys.ENTER);
    }

    public Performable selectOptionParentUnit(String option){
        return Enter.theValue(option).into(inputSearchParentUnit).thenHit(Keys.ENTER);
    }

    public Performable clickButtonSingIn(){
        return Click.on(singIn);
    }

    public Performable clickButtonToggler(){ return Click.on(buttonToggler); }

    public Performable clickButtonOrganization(){ return Click.on(buttonOrganization); }

    public Performable clickButtonBusinessUnits(){ return Click.on(buttonBusinessUnits); }

    public Performable clickButtonNewBusinessUnits(){ return Click.on(buttonNewBusinessUnits); }

    public Performable clickButtonSave(){ return  Click.on(buttonSave); }

    public Performable clickButtonMeeting(){ return Click.on(buttonMeeting); }

    @PostConstruct
    void onPostConstruct() {
        iconChoucair = Target.the(ConstantsChoucair.LOGO_CHOUCAIR).located(By.xpath(ConstantsChoucair.LOGO_CHOUCAIR));
        LabelLoginToYourAccount = Target.the(ConstantsChoucair.LOGIN_TO_YOUR_ACCOUNT).located(By.xpath(ConstantsChoucair.LOGIN_TO_YOUR_ACCOUNT));
        buttonForgotPassword = Target.the(ConstantsChoucair.FORGOT_PASSWORD).located(By.xpath(ConstantsChoucair.FORGOT_PASSWORD));
        singIn = Target.the(ConstantsChoucair.INICIAR_SESION).located(By.xpath(ConstantsChoucair.INICIAR_SESION));
        inputUserName = Target.the(ConstantsChoucair.INPUT_USERNAME).located(By.xpath(ConstantsChoucair.INPUT_USERNAME));
        inputPassword= Target.the(ConstantsChoucair.INPUT_PASSWORD).located(By.xpath(ConstantsChoucair.INPUT_PASSWORD));
        buttonToggler = Target.the(ConstantsChoucair.BUTTON_TOGGLER).located(By.xpath(ConstantsChoucair.BUTTON_TOGGLER));
        buttonOrganization = Target.the(ConstantsChoucair.BUTTON_ORGANIZATION).located(By.xpath(ConstantsChoucair.BUTTON_ORGANIZATION));
        buttonBusinessUnits = Target.the(ConstantsChoucair.BUTTON_BUSINESS_UNITS).located(By.xpath(ConstantsChoucair.BUTTON_BUSINESS_UNITS));
        buttonNewBusinessUnits = Target.the(ConstantsChoucair.BUTTON_NEW_BUSINESS_UNKT).located(By.xpath(ConstantsChoucair.BUTTON_NEW_BUSINESS_UNKT));
        inputName = Target.the(ConstantsChoucair.INPUT_NAME).located(By.xpath(ConstantsChoucair.INPUT_NAME));
        parentUnit = Target.the(ConstantsChoucair.SELECT_PARENT_UNIT).located(By.xpath(ConstantsChoucair.SELECT_PARENT_UNIT));
        buttonSave = Target.the(ConstantsChoucair.BUTTON_SAVE).located(By.xpath(ConstantsChoucair.BUTTON_SAVE));
        inputSearchParentUnit = Target.the(ConstantsChoucair.INPUT_SEARCH_PARENT_UNIT).located(By.xpath(ConstantsChoucair.INPUT_SEARCH_PARENT_UNIT));
        buttonMeeting = Target.the(ConstantsChoucair.BUTTON_MEETING).located(By.xpath(ConstantsChoucair.BUTTON_MEETING));
    }
}
