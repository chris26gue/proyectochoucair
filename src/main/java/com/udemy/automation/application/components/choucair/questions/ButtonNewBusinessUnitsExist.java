package com.udemy.automation.application.components.choucair.questions;

import com.udemy.automation.application.ActionsUser.UserActions;
import com.udemy.automation.application.components.choucair.ChoucairComponent;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.springframework.beans.factory.annotation.Autowired;

public class ButtonNewBusinessUnitsExist implements Question<Boolean> {

    @Autowired
    private ChoucairComponent component;

    @Override
    public Boolean answeredBy(Actor actor){
        return UserActions.isPresent(component.getButtonNewBusinessUnits(), actor);
    }

}
