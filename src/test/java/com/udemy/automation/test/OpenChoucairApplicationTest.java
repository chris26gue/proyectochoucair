package com.udemy.automation.test;

import com.udemy.automation.application.components.choucair.questions.*;
import com.udemy.automation.application.components.browser.OpenBrowser;
import com.udemy.automation.application.components.choucair.tasks.*;
import com.udemy.automation.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.*;

/**
 * @author Christian D Moreno Guerrero.
 */
public class OpenChoucairApplicationTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;
    private Actor actor = Actor.named("Christian");
    private OpenBrowser openBrowser;
    private ImageLogoExist imageLogoExist;
    private ForgotPasswordExist forgotPasswordExist;
    private LoginToYourAccountExist loginToYourAccountExist;
    private InputUserNameExist inputUserNameExist;
    private InsertCredentialsChoucair insertCredentialsChoucair;
    private ButtonTogglerExist buttonTogglerExist;
    private ClickButtonTogglerChoucair clickButtonTogglerChoucair;
    private ButtonOrganizationExist buttonOrganizationExist;
    private ClickButtonOrganizationChoucair clickButtonOrganizationChoucair;
    private ButtonBusinessUnitsExist buttonBusinessUnitsExist;
    private ClickButtonBusinessUnitsChoucair clickButtonBusinessUnitsChoucair;
    private ButtonNewBusinessUnitsExist buttonNewBusinessUnitsExist;
    private ClickNewBusinessUnitsChoucair clickNewBusinessUnitsChoucair;
    private InsertValuesNewBusinessUnitaChoucair insertValuesNewBusinessUnitaChoucair;
    private ButtonMeetingExist buttonMeetingExist;
    private ClickButtonMeetingChoucair clickButtonMeetingChoucair;

    @Before
    public void setup() {
        initializeActor(actor);

        openBrowser = taskInstance(OpenBrowser.class);
        insertCredentialsChoucair = taskInstance(InsertCredentialsChoucair.class);
        insertCredentialsChoucair.setUserName(config.getUserName());
        insertCredentialsChoucair.setPassword(config.getPassword());
        clickButtonTogglerChoucair = taskInstance(ClickButtonTogglerChoucair.class);
        clickButtonOrganizationChoucair = taskInstance(ClickButtonOrganizationChoucair.class);
        clickButtonBusinessUnitsChoucair = taskInstance(ClickButtonBusinessUnitsChoucair.class);
        clickNewBusinessUnitsChoucair = taskInstance(ClickNewBusinessUnitsChoucair.class);
        insertValuesNewBusinessUnitaChoucair = taskInstance(InsertValuesNewBusinessUnitaChoucair.class);
        insertValuesNewBusinessUnitaChoucair.setName(config.getName());
        insertValuesNewBusinessUnitaChoucair.setParentUnit(config.getParentUnit());
        clickButtonMeetingChoucair = taskInstance(ClickButtonMeetingChoucair.class);


        imageLogoExist = questionInstance(ImageLogoExist.class);
        forgotPasswordExist = questionInstance(ForgotPasswordExist.class);
        loginToYourAccountExist = questionInstance(LoginToYourAccountExist.class);
        inputUserNameExist = questionInstance(InputUserNameExist.class);
        buttonTogglerExist = questionInstance(ButtonTogglerExist.class);
        buttonOrganizationExist = questionInstance(ButtonOrganizationExist.class);
        buttonBusinessUnitsExist = questionInstance(ButtonBusinessUnitsExist.class);
        buttonNewBusinessUnitsExist = questionInstance(ButtonNewBusinessUnitsExist.class);
        buttonMeetingExist = questionInstance(ButtonMeetingExist.class);
    }

    @WithTag("openApplicationChoucair")
    @Test
    public void userOpenApplication() {
        givenThat(actor)
                .attemptsTo(openBrowser);

        then(actor).should(
                seeThat(imageLogoExist),
                seeThat(loginToYourAccountExist),
                seeThat(forgotPasswordExist),
                seeThat(inputUserNameExist)
        );

        when(actor).attemptsTo(
                insertCredentialsChoucair
        );

        then(actor).should(
                seeThat(buttonTogglerExist)
        );

        when(actor).attemptsTo(
                clickButtonTogglerChoucair
        );


        then(actor).should(
                seeThat(buttonOrganizationExist)
        );

        when(actor).attemptsTo(
                clickButtonOrganizationChoucair
        );

        then(actor).should(
                seeThat(buttonBusinessUnitsExist)
        );

        when(actor).attemptsTo(
                clickButtonBusinessUnitsChoucair
        );

        then(actor).should(
                seeThat(buttonNewBusinessUnitsExist)
        );

        when(actor).attemptsTo(
                clickNewBusinessUnitsChoucair
        );

        when(actor).attemptsTo(
                insertValuesNewBusinessUnitaChoucair
        );

        then(actor).should(
                seeThat(buttonTogglerExist)
        );

        when(actor).attemptsTo(
                clickButtonTogglerChoucair
        );

        then(actor).should(
                seeThat(buttonMeetingExist)
        );

        when(actor).attemptsTo(
                clickButtonMeetingChoucair
        );
    }
}
